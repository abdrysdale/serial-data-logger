#!/usr/bin/env python3

# Python imports
import os
import sys
import logging
import time
from multiprocessing import Process, Manager

# Local import
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BASE_DIR)
import serialdatalog as sdl

logging.basicConfig(level="INFO")
logger = logging.getLogger(__name__)

def test_trigger():

    def f(trigger, n):
        while trigger.value:
            logger.info(f"Trigger is {trigger.value},  sleeping for {n}s")
            time.sleep(n)
        return True


    with Manager() as manager:
        trigger = manager.Value('b', True)
        p = Process(target=f, args=(trigger, 1,))
        p.start()
        time.sleep(1)
        trigger.value = False
        time.sleep(1)
        if p.is_alive():
            p.terminate()
            logger.critical("Trigger did not cause function to exit.")
            assert False
        p.join()

    with Manager() as manager:
        table_dict = {"a" : "REAL", "b" : "REAL", "c" : "REAL", "d" : "REAL", "e" : "REAL"}
        db_path = "test.sqlite3"

        trigger = manager.Value('b', True)
        p = Process(target=sdl.logger, args=(table_dict,), kwargs={"dest" : db_path, "trigger" : trigger})
        p.start()
        time.sleep(1)
        trigger.value = False
        time.sleep(1)
        if p.is_alive():
            p.terminate()
            logger.critical("Trigger did not cause logger to exit.")
            assert False
        p.join()
        os.remove(db_path)

    logger.info("Oh, I'm afraid the trigger will be quite operational when your friends arrive!")


if __name__ == "__main__":
    test_trigger()
