"""An example script for logging a finometer output"""

# Python Imports
import sys
import os
import logging

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BASE_PATH)
import serialdatalog as sdl

logging.basicConfig(level=logging.INFO)
table_dict = {
    "Elapsed_Time" : "REAL",
    "Finger_Pressure" : "REAL",
    "Height" : "REAL",
    "Arm_Pressure" : "REAL",
    "Finger_Plethysmogram" : "REAL",
}
sdl.logger(table_dict)
